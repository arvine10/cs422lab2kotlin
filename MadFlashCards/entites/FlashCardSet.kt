package entities

data class FlashCardSet(val title: String) {

    fun flashCarSetdCollection(): Array<FlashCardSet> {
        val flashCardSet1 = FlashCardSet("math")
        val flashCardSet2 = FlashCardSet("history")
        val flashCardSet3 = FlashCardSet("business")
        val flashCardSet4 = FlashCardSet("english")
        val flashCardSet5 = FlashCardSet("computer science")
        val flashCardSet6 = FlashCardSet("philiosophy")
        val flashCardSet7 = FlashCardSet("astronomy")
        val flashCardSet8 = FlashCardSet("music")
        val flashCardSet9 = FlashCardSet("art")
        val flashCardSet10 = FlashCardSet("biology")
        val flashCardSet_list = arrayOf(flashCardSet1,flashCardSet2,flashCardSet3,flashCardSet4,
            flashCardSet5,flashCardSet6,flashCardSet7,flashCardSet8,flashCardSet9,flashCardSet10)
        return flashCardSet_list
    }



}